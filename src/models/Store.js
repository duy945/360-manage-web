import { Region } from './Region';
export class Store {

  constructor({ id = null, name = '', sid = null, region_id = null, address = [], region = Array(Region) }) {
    this.id = id;
    this.name = name;
    this.sid = sid;
    this.region_id = region_id;
    this.address = address;
    this.region = region;
  }

}
