import { Merchant } from './Merchant';
export class User {

  constructor({ id = null, email = '', first_name = '', last_name = '', addresses = [], merchants = Array(Merchant), roles = [], mobile = '' }) {
    this.id = id;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.addresses = addresses;
    this.merchants = merchants;
    this.roles = roles;
    this.mobile = mobile;
  }

}
