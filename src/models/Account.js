class Account {

  constructor() {
    this.name = '';
    this.first_name = '';
    this.last_name = '';
    this.email = '';
    this.id = '';
    this.picture = {};
  }


  getName() {
    return this.name;
  }

  setName(value) {
    this.name = value;
  }

  getFirstName() {
    return this.first_name;
  }

  setFirstName(value) {
    this.first_name = value;
  }

  getLastName() {
    return this.last_name;
  }

  setLastName(value) {
    this.last_name = value;
  }

  getEmail() {
    return this.email;
  }

  setEmail(value) {
    this.email = value;
  }

  getId() {
    return this.id;
  }

  setId(value) {
    this.id = value;
  }

  getPicture() {
    return this.picture;
  }

  setPicture(value) {
    this.picture = value;
  }

  mappingAccount(_acc) {
    const acc = new Account();
    acc.setName(_acc.name);
    acc.setId(_acc.id);
    acc.setEmail(_acc.email);
    acc.setFirstName(_acc.first_name);
    acc.setLastName(_acc.last_name);
    acc.setPicture(_acc.picture);
    return acc;
  }

}

module.exports = new Account();
