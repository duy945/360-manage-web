import { Price } from './Price';
export class Product {

  constructor(
        {
            id = null, name = '', product_code = '', bar_code = '', sku_no = '', category_id = null, description = '',
            ean_upc = '', image_url = '', measure_amount = null, measure_id = null, allergy = '', tax_id = null,
            addons = null, prices = Array(Price), stocks = null,
        }
    ) {

  }

}
