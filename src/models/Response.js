class Response {

  constructor() {
    this.code = '';
    this.success = '';
    this.data = {};
    this.message = '';
  }

  setCode(code) {
    this.code = code;
  }
  setSuccess(success) {
    this.success = success;
  }
  setData(data) {
    this.data = data;
  }
  setMessage(msg) {
    this.message = msg;
  }

  getCode() {
    return this.code;
  }
  getSuccess() {
    return this.success;
  }
  getData() {
    return this.data;
  }
  getMessage() {
    return this.message;
  }

  returnFailed(code, msg) {
    const response = new Response();
    response.setCode(code);
    response.setSuccess(false);
    response.setMessage(msg);
    return response;
  }

  returnSuccess(data, msg) {
    const response = new Response();
    response.setCode(200);
    response.setSuccess(true);
    response.setData(data);
    response.setMessage(msg);
    return response;
  }
}

module.exports = new Response();
