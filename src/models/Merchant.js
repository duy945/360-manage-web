import { Store } from './Store';
export class Merchant {

  constructor({ id = null, mid = null, tname = '', phone = '', logo_url = '', business_type = '', stores = Array(Store) }) {
    this.id = id;
    this.mid = mid;
    this.tname = tname;
    this.phone = phone;
    this.logo_url = logo_url;
    this.business_type = business_type;
    this.stores = stores;
  }

}
