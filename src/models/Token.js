import { User } from './User';
export class Token {

  constructor({ token_type = null, expires_in = null, access_token = '', refresh_token = '', user = User }) {
    this.token_type = token_type;
    this.expires_in = expires_in;
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.user = user;
    this.created_at = Date.now();
  }

}
