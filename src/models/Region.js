export class Region {

  constructor({ id = null, region_name = '', store_id = null, merchant_id = null }) {
    this.id = id;
    this.region_name = region_name;
    this.store_id = store_id;
    this.merchant_id = merchant_id;
  }

}
