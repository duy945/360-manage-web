/*
* Using to authenticate any component before rendering
*/
import React from 'react';
import { connect } from 'react-redux';

const authenticate = (Component) => {
  class CheckAuthenticate extends React.Component {
    componentWillMount() {
      window.location.href = '/#/login';
    }
    render() {
    	return (
      	<Component {...this.props} />
  		);
    }
	}

  const mapStateToProps = (state) => ({
    user: state.auth,
  });
  return connect(mapStateToProps)(CheckAuthenticate);
};

export default authenticate;
