import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from './actions';

const LoginPage = ({login}) => (
  <div onClick={() => login()}>
    LoginPage
  </div>
);

LoginPage.defaultProps = {
  login: () => {
  },
};
LoginPage.propTypes = {
  login: PropTypes.func,
};

export default connect(undefined, {
  login,
})(LoginPage);
