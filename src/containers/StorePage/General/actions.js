import gql from 'graphql-tag';
import { LOGIN_MUTATION } from 'apis/Queries';
import { apolloClient } from 'apollo';

const loginMutation = gql`${LOGIN_MUTATION}`;

export const login = ({ username, password }) => () => {
  apolloClient.mutate({
    mutation: loginMutation,
    variables: { user: { username, password } },
  })
  .then((result) => {
  	console.info(result.data.logIn);
  });
};
