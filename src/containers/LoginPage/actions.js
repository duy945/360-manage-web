import gql from 'graphql-tag';
import { toastr } from 'react-redux-toastr';
import { apolloClient } from 'apollo';
import { Token } from 'models/Token';
import { LOGIN_MUTATION, RESET_PASSWORD_MUTATION, SEND_RESET_CODE_MUTATION } from 'apis/Queries';

export const ActionTypes = {};

const loginMutation = gql`${LOGIN_MUTATION}`;

ActionTypes.LOGIN_REQUEST = 'LOGIN_PAGE/LOGIN_REQUEST';
ActionTypes.LOGIN_SUCCESS = 'LOGIN_PAGE/LOGIN_SUCCESS';
ActionTypes.LOGIN_FAILURE = 'LOGIN_PAGE/LOGIN_FAILURE';
export const login = ({ username = 'demo@biits.io', password = 'password' }) => (dispatch) => {
  dispatch({ type: ActionTypes.LOGIN_REQUEST });
  apolloClient.mutate({
    mutation: loginMutation,
    variables: { user: { username, password } },
  })
  .then((result) => {
    toastr.success('Login success');
    dispatch({ type: ActionTypes.LOGIN_SUCCESS, payload: new Token(result.data.logIn) });
  })
  .catch((err) => {
    toastr.error('Login failure');
    console.error(err);
  });
};

const resetPasswordMutation = gql`${RESET_PASSWORD_MUTATION}`;
export const resetPassword = ({ code, password, confirmPassword }) => (dispatch) => {
  apolloClient.mutate({
    mutation: resetPasswordMutation,
    variables: { input: { code, password, confirmPassword } },
  })
  .then(() => {
  })
  .catch((err) => {
    console.error(err);
  });
};

const sendCodeMutation = gql`${SEND_RESET_CODE_MUTATION}`;
export const sendCode = ({ email }) => (dispatch) => {
  apolloClient.mutate({
    mutation: sendCodeMutation,
    variables: { input: { email } },
  })
  .then(() => {
  })
  .catch((err) => {
    console.error(err);
  });
};
