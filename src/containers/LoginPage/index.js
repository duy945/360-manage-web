import React from 'react';
import { connect } from 'react-redux';
import Login from 'components/LoginPage/Login';
import Claim from 'components/LoginPage/Claim';
import { login } from './actions';
import './loginPage.scss';

const LoginPage = ({ login }) => (
  <div className="login-wrapper">
    <div className="login">
      <div className="logo">
        <img src={require('../../../assets/images/Biits-Logo.png')} alt="" />
      </div>
      <div className="col-md-6 claim no-padding">
        <Claim />
      </div>
      <div className="col-md-6 login-part no-padding">
        <Login
          onSubmit={login}
        />
      </div>
      <div className="clearfix"></div>
      <div className="footer-block">United Kingdom</div>
    </div>
  </div>
);

export default connect(undefined, {
  login,
})(LoginPage);
