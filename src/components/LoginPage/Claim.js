import React from 'react';

const Claim = () => (
  <div className="claim-area">
    <div className="title">New to biits?</div>
    <div className="sub-title">If you have not yet claimed your business click the button below</div>
    <div className="biits-btn btn-primary-blue">
      CLAIM YOUR BUSINESS
    </div>
  </div>
);

export default Claim;
