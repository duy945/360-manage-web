## Introduce
 Project using React combine with Redux js in real world 

## Technology
 * React
 * Redux
 * Admin LTE

 ## Installation
**Required**:
    - **nodejs** - version: **5+**
    - **webpack**
## Run
1. Open **terminal** or **cmd** in project directory, run `npm install` or `yarn install`. This will install all dependencies.
2. Run `npm start` or `yarn start` to start dev mode. 
App will run on port 3000
## Contributors

[DuyPT](https://github.com/steThera)

## License

This project is licensed under the MIT license, Copyright (c) 2017 steThera. 
For more information see `LICENSE`.